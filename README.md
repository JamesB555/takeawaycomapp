# RestaurantsListApp

It was said in the task to visualize the selected sort and the sort value. 
But if put all eight sorting values into the item's layout, it will look messy. So I added only some of the SV into layout (rating average, delivery cost)
Here's a screenshot:

https://drive.google.com/open?id=17eoMPeFdjALuRkBrKRg1J5y_WdMlA_LE 

To test all the non-visualized options I've added an additional util com.eugene.pekutovskiy.restaurantslistapp.domain.util.PrintSortDataUtil.kt
Just select some sort option in UI and It will print sort option, restaurant name, openings state and the sorting value such way:	

https://drive.google.com/open?id=1xePJAS1HpbXeKo37vVUrQPWDLgoqUUu6
 



